package action

import (
	"bitbucket.org/romanoff/ahc/jsdemo"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

var jsDemos = make([]*jsdemo.File, 0)

func visitJsDemo(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, ".demo") {
			jsDemos = append(jsDemos, &jsdemo.File{Path: path})
		}
	}
	return nil
}

func GenerateJsDemos(jsComponentsPath string, htmlComponentsPath string) error {
	err := ReadComponents(htmlComponentsPath)
	if err != nil {
		return err
	}
	err = filepath.Walk(jsComponentsPath, visitJsDemo)
	if err != nil {
		return err
	}
	for _, jsDemo := range jsDemos {
		err = jsDemo.Parse(components)
		if err != nil {
			return err
		}
	}

	tempfile, err := ioutil.TempFile("", "gogen-jsdemos")
	if err != nil {
		return err
	}
	defer os.Remove(tempfile.Name())
	defer tempfile.Close()
	_, err = tempfile.WriteString("ahc jsdemos\n")
	if err != nil {
		return err
	}
	demoParams := make([]string, 0)
	demoParams = append(demoParams, strconv.Itoa(len(jsDemos)))
	for _, jsDemo := range jsDemos {
		demoParams = append(demoParams, jsDemo.Title)
		demoParams = append(demoParams, strings.Join(jsDemo.Deps, "|"))
		demoParams = append(demoParams, strings.Join(jsDemo.EventTypes, "|"))
		demoParams = append(demoParams, jsDemo.Content)
		demoParams = append(demoParams, jsDemo.Css)
		demoParams = append(demoParams, jsDemo.InitScript)
		demoParams = append(demoParams, jsDemo.Body)
	}
	bytes, _ := xml.MarshalIndent(&Params{QueryString: demoParams}, "", "  ")
	_, err = tempfile.WriteString(string(bytes))
	if err != nil {
		return err
	}
	cmd := exec.Command("gogen", "file", tempfile.Name())
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	fmt.Println(string(output))
	return nil
}
