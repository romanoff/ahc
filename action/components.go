package action

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var components = make([]*stylesheet.Component, 0)

func visit(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, "css") && !isIgnoredFile(filename) {
			components = append(components, &stylesheet.Component{File: f, Path: path})
		}
	}
	return nil
}

func ReadComponents(path string) error {
	err := filepath.Walk(path, visit)
	if err != nil {
		return err
	}
	allProvides := make(map[string]string)
	for _, component := range components {
		component.Init()
		for _, provide := range component.Provides {
			if allProvides[provide] != "" {
				return errors.New(fmt.Sprintf("Same provides in %v and %v files - %v", allProvides[provide], component.Path, provide))
			}
			allProvides[provide] = component.Path
		}
	}
	deps := stylesheet.BuildDeps(components)
	for _, component := range components {
		err = component.Compile(components, deps)
		if err != nil {
			return err
		}
	}
	return nil
}

func GetTemplatesAvailability(extensions []string) {
	for _, component := range components {
		for _, extension := range extensions {
			component.CheckTemplate(extension)
		}
	}
}
