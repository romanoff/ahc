package action

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"bitbucket.org/romanoff/ahc/view"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

var views []*view.Item
var layouts []*view.Layout
var partialComponents = make(map[string][]string)

func GenerateViews() error {
	templatesInfo, err := view.GetTemplatesInfo()
	if err != nil {
		return err
	}
	defaultCss := getDefaultCss()
	componentsPaths, err := getComponentsPaths()
	if err != nil {
		return err
	}
	for _, componentsPath := range componentsPaths {
		if err := ReadComponents(componentsPath); err != nil {
			return err
		}
	}
	extensions := make([]string, 0)
	for _, templateInfo := range templatesInfo {
		extensions = append(extensions, templateInfo.Extension)
	}
	GetTemplatesAvailability(extensions)
	path, err := os.Getwd()
	if err != nil {
		return err
	}
	err = filepath.Walk(path, visitLayout)
	if err != nil {
		return err
	}
	err = filepath.Walk(path, visitView)
	if err != nil {
		return err
	}
	partials := make(map[string]string)
	for _, viewItem := range views {
		if viewItem.Partial {
			err := viewItem.GenerateHtml(layouts, components, templatesInfo)
			if err != nil {
				return err
			}
			filename := viewItem.File.Name()
			filename = strings.TrimLeft(filename, "_")
			filename = filename[0 : len(filename)-len(".view")]
			partials[filename] = viewItem.Html
		}
	}

	for i := len(views) - 1; i >= 0; i-- {
		viewItem := views[i]
		if viewItem.Partial {
			filename := viewItem.File.Name()
			filename = strings.TrimLeft(filename, "_")
			filename = filename[0 : len(filename)-len(".view")]
			partialComponents[filename] = viewItem.UsedComponents
			views = append(views[:i], views[i+1:]...)
		}
	}

	for _, viewItem := range views {
		err := viewItem.GenerateHtmlWithPartials(layouts, components, templatesInfo, partials)
		if err != nil {
			return err
		}
	}

	if err = os.RemoveAll(path + "/views"); err != nil {
		return err
	}

	viewParams := make([]string, 0)
	viewParams = append(viewParams, defaultCss)
	viewParams = append(viewParams, getViewsCss())
	viewParams = append(viewParams, strconv.Itoa(len(views)))
	for _, viewItem := range views {
		viewParams = append(viewParams, viewItem.Name())
		viewParams = append(viewParams, viewItem.Html)
	}
	tempfile, err := ioutil.TempFile("", "gogen-views")
	defer os.Remove(tempfile.Name())
	defer tempfile.Close()
	_, err = tempfile.WriteString("ahc views\n")
	if err != nil {
		return err
	}
	bytes, _ := xml.MarshalIndent(&Params{QueryString: viewParams}, "", "  ")
	_, err = tempfile.WriteString(string(bytes))
	if err != nil {
		return err
	}
	cmd := exec.Command("gogen", "file", tempfile.Name())
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	fmt.Println(string(output))
	return nil
}

func visitLayout(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, "layout") && !isIgnoredFile(filename) {
			layout := &view.Layout{Path: path, File: f}
			err = layout.LoadContent()
			if err != nil {
				return err
			}
			layouts = append(layouts, layout)

		}
	}
	return nil
}

func visitView(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, "view") && !isIgnoredFile(filename) {
			view := &view.Item{Path: path, File: f}
			if strings.HasPrefix(filename, "_") {
				view.Partial = true
			}
			views = append(views, view)

		}
	}
	return nil
}

func getViewsCss() string {
	viewsCss := make(map[string]string)
	for _, viewItem := range views {
		for _, componentName := range viewItem.UsedComponents {
			if viewsCss[componentName] == "" {
				component, _ := stylesheet.FindComponent(components, componentName)
				viewsCss[componentName] = component.CompiledContent

			}
		}
		for _, partialName := range viewItem.UsedPartials {
			for _, componentName := range partialComponents[partialName] {
				if viewsCss[componentName] == "" {
					component, _ := stylesheet.FindComponent(components, componentName)
					viewsCss[componentName] = component.CompiledContent
				}
			}
		}
	}
	allCss := ""
	for _, viewCss := range viewsCss {
		allCss += viewCss
	}
	return allCss
}
