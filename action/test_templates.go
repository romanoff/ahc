package action

import (
	"bitbucket.org/romanoff/ahc/ti"
	"bitbucket.org/romanoff/ahc/view"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type TestFile struct {
	Path    string
	Content string
	Tests   []*TemplateTest
}

func (self *TestFile) ReadContent() error {
	content, err := ioutil.ReadFile(self.Path)
	if err != nil {
		return err
	}
	self.Content = string(content)
	return nil
}

const (
	INPUT = iota
	EXPECTED
)

const (
	Reset      = "\x1b[0m"
	Bright     = "\x1b[1m"
	Dim        = "\x1b[2m"
	Underscore = "\x1b[4m"
	Blink      = "\x1b[5m"
	Reverse    = "\x1b[7m"
	Hidden     = "\x1b[8m"

	FgBlack   = "\x1b[30m"
	FgRed     = "\x1b[31m"
	FgGreen   = "\x1b[32m"
	FgYellow  = "\x1b[33m"
	FgBlue    = "\x1b[34m"
	FgMagenta = "\x1b[35m"
	FgCyan    = "\x1b[36m"
	FgWhite   = "\x1b[37m"

	BgBlack   = "\x1b[40m"
	BgRed     = "\x1b[41m"
	BgGreen   = "\x1b[42m"
	BgYellow  = "\x1b[43m"
	BgBlue    = "\x1b[44m"
	BgMagenta = "\x1b[45m"
	BgCyan    = "\x1b[46m"
	BgWhite   = "\x1b[47m"
)

var re *regexp.Regexp = regexp.MustCompile("\\s")

func (self *TestFile) ParseContent() error {
	lines := strings.Split(self.Content, "\n")
	json := ""
	expected := ""
	mode := -1
	for _, line := range lines {
		if strings.TrimSpace(line) == "Input:" {
			mode = INPUT
			if json != "" && expected != "" {
				self.Tests = append(self.Tests, &TemplateTest{Json: json, Html: re.ReplaceAllString(expected, "")})
			}
			json = ""
			expected = ""
		} else if strings.TrimSpace(line) == "Expected:" {
			mode = EXPECTED
		} else {
			if mode == INPUT {
				json += line
			}
			if mode == EXPECTED {
				expected += line
			}
		}
	}
	if mode == EXPECTED && json != "" && expected != "" {
		self.Tests = append(self.Tests, &TemplateTest{Json: json, Html: re.ReplaceAllString(expected, "")})
	}
	return nil
}

type TemplateTest struct {
	Json string
	Html string
}

func (self *TemplateTest) TestTemplate(n int, cssPath string, path string, command string) {
	_, err := os.Stat(path)
	if err != nil {
		return
	}
	provide := self.getProvide(cssPath)
	output, _ := view.RenderTemplate(command, provide, path, self.Json)
	if self.Html != re.ReplaceAllString(string(output), "") {
		fmt.Println("Expected:")
		fmt.Println(self.Html)
		fmt.Println("Got:")
		fmt.Println(string(output))
		fmt.Printf("\n"+Bright+FgRed+"%v test for %v template doesn't pass\n"+Reset, n, path)
	} else {
		fmt.Printf(Bright + FgGreen + "." + Reset)
	}
}

var provideRe *regexp.Regexp = regexp.MustCompile("@provide\\s+['\"](.+)['\"]")

func (self *TemplateTest) getProvide(cssPath string) string {
	bytesContent, _ := ioutil.ReadFile(cssPath)
	content := string(bytesContent)
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		matches := provideRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			return matches[1]
		}
	}
	return ""
}

var testFiles []*TestFile

func visitTest(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, ".test") {
			testFile := &TestFile{Path: path}
			err := testFile.ReadContent()
			if err != nil {
				return err
			}
			err = testFile.ParseContent()
			if err != nil {
				fmt.Println("Parsing of %v test file failed", path)
				os.Exit(1)
			}
			testFiles = append(testFiles, testFile)
		}
	}
	return nil
}

func TestTemplates() error {
	path, err := os.Getwd()
	if err != nil {
		return err
	}
	err = filepath.Walk(path, visitTest)
	if err != nil {
		return err
	}
	templatesInfo, err := view.GetTemplatesInfo()
	if err != nil {
		return err
	}
	templateExtensions := make([]string, 0)
	for _, templateInfo := range templatesInfo {
		templateExtensions = append(templateExtensions, templateInfo.Extension)
	}
	for _, testFile := range testFiles {
		for _, extension := range templateExtensions {
			for n, test := range testFile.Tests {
				cssPath := strings.Replace(testFile.Path, ".test", ".css", 1)
				command := GetCommandForExtension(templatesInfo, extension)
				test.TestTemplate(n, cssPath, strings.Replace(testFile.Path, ".test", "."+extension, 1), command)
			}
		}
	}
	fmt.Printf("\n")
	return nil
}

func GetCommandForExtension(templatesInfo []*ti.TemplateInfo, extension string) string {
	for _, templateInfo := range templatesInfo {
		if templateInfo.Extension == extension {
			return templateInfo.Command
		}
	}
	return ""
}
