package action

import (
	"fmt"
	"testing"
)

func TestParseTestFileContent(t *testing.T) {
	content := `
Input: 
{imageUrl: 'image.png', name: 'product', url: '#', companyName: 'Company', compayUrl: 'google.com', productLinkId: '1', companyLInkId: '2'}
Expected: 
<div class="p_application">
  <div class="p_application_image_container">
    <img class="p_application_image_container" src="image.png" />
  </div>
  <div class="p_application_content">
    <div class="p_application_content_header">
      <a class="p_application_content_header_link" id="1" href="#">product</a>
    </div>
    <div class="p_application_content_company">
      <a href="google.com" class="p_application_content_company_link" id="2">Compay</a>
    </div>
    <div class="p_application_content_rating"></div>
  </div>
</div>
`
	testFile := &TestFile{Content: content}
	testFile.ParseContent()
	if len(testFile.Tests) != 1 {
		t.Errorf("Expected to get 1 test, but got %v", len(testFile.Tests))
	}
	fmt.Println(testFile.Tests[0])
}
