package action

import "errors"

func PerformAction(args []string) error {
	if len(args) == 1 && args[0] == "demos" {
		return GenerateDemos()
	}
	if len(args) == 1 && args[0] == "views" {
		return GenerateViews()
	}
	if len(args) == 1 && args[0] == "test" {
		return TestTemplates()
	}
	if len(args) == 3 && args[0] == "jsdemos" {
		return GenerateJsDemos(args[1], args[2])
	}
	return errors.New("No action was performed")
}
