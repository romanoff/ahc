package action

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"strings"
)

type AhcConfig struct {
	DefaultCss      []string `json:"default_css"`
	ComponentsPaths []string `json:"components"`
}

var ahcConfig *AhcConfig

func getAhcConfig() (*AhcConfig, error) {
	if ahcConfig != nil {
		return ahcConfig, nil
	}
	bytesContent, err := ioutil.ReadFile("ahc.config")
	if err != nil {
		return nil, errors.New("Reading ahc config error")
	}
	config := &AhcConfig{}
	err = json.Unmarshal(bytesContent, config)
	if err != nil {
		return nil, err
	}
	ahcConfig = config
	return ahcConfig, nil
}

func getDefaultCss() string {
	config, err := getAhcConfig()
	if err != nil {
		return ""
	}
	defaultCss := make([]string, 0)
	for _, cssPath := range config.DefaultCss {
		var bytesContent []byte
		bytesContent, err = ioutil.ReadFile(cssPath)
		if err == nil {
			defaultCss = append(defaultCss, string(bytesContent))
		}
	}
	return strings.Join(defaultCss, "\n")
}

func getComponentsPaths() ([]string, error) {
	config, err := getAhcConfig()
	if err != nil {
		return nil, err
	}
	if config.ComponentsPaths == nil || len(config.ComponentsPaths) == 0 {
		return nil, errors.New("No component paths found")
	}
	return config.ComponentsPaths, nil
}
