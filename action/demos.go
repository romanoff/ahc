package action

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func isIgnoredFile(filename string) bool {
	return strings.HasPrefix(filename, ".")
}

func GenerateDemos() error {
	path, err := os.Getwd()
	if err != nil {
		return err
	}
	if err = os.RemoveAll(path + "/demos"); err != nil {
		return err
	}
	err = ReadComponents(path)
	if err != nil {
		return err
	}
	generateDemosWithGogen(components)
	return nil
}

type Params struct {
	QueryString []string
}

func generateDemosWithGogen(components []*stylesheet.Component) error {
	componentParams := make([]string, 0)
	componentsWithHtml := 0
	for _, component := range components {
		html := component.GetHtml()
		additionalCss, additionalFonts := getAdditionalInfo(html)
		if component.Font != "" {
			if additionalFonts == "" {
				additionalFonts = component.Font
			} else {
				additionalFonts += "|" + component.Font
			}
		}
		if html != "" {
			componentsWithHtml++
			componentParams = append(componentParams, component.Provides[0])
			componentParams = append(componentParams, additionalFonts)
			componentParams = append(componentParams, component.CompiledContent)
			componentParams = append(componentParams, additionalCss)
			componentParams = append(componentParams, html)
		}
	}
	params := make([]string, len(componentParams)+2)
	params[0] = getDefaultCss()
	params[1] = strconv.Itoa(componentsWithHtml)
	copy(params[2:], componentParams)
	tempfile, err := ioutil.TempFile("", "gogen-demos")
	defer os.Remove(tempfile.Name())
	defer tempfile.Close()
	_, err = tempfile.WriteString("ahc demos\n")
	if err != nil {
		return err
	}
	bytes, _ := xml.MarshalIndent(&Params{QueryString: params}, "", "  ")
	_, err = tempfile.WriteString(string(bytes))
	if err != nil {
		return err
	}
	cmd := exec.Command("gogen", "file", tempfile.Name())
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	fmt.Println(string(output))
	return nil
}

func getAdditionalInfo(html string) (css string, fonts string) {
	lines := strings.Split(html, "\n")
	requires := make([]string, 0)
	for _, line := range lines {
		matches := stylesheet.RequireRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			requires = append(requires, matches[1])
		}
	}
	additionalCss := ""
	additionalFonts := make([]string, 0)
	for _, require := range requires {
		for _, component := range components {
			for _, provide := range component.Provides {
				if provide == require {
					additionalCss += component.CompiledContent
					if component.Font != "" {
						additionalFonts = append(additionalFonts, component.Font)
					}
				}
			}
		}
	}
	return additionalCss, strings.Join(additionalFonts, "|")
}
