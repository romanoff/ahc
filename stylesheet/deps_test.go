package stylesheet

import (
	"reflect"
	"testing"
)

func TestBuildDeps(t *testing.T) {
	components := []*Component{
		&Component{Provides: []string{"d"}, Requires: []string{"c"}},
		&Component{Provides: []string{"c"}, Requires: []string{"b"}},
		&Component{Provides: []string{"b"}, Requires: []string{"a"}},
		&Component{Provides: []string{"a"}},
	}
	deps := BuildDeps(components)
	if len(deps["a"]) > 0 {
		t.Errorf("Expected 'a' to have no deps, but found %v", deps["a"])
	}
	if !reflect.DeepEqual(deps["b"], []string{"a"}) {
		t.Errorf("Expected 'b' to have 'a' deps, but found %v", deps["b"])
	}
	if !reflect.DeepEqual(deps["c"], []string{"b", "a"}) {
		t.Errorf("Expected 'c' to have 'b', 'a' deps, but found %v", deps["c"])
	}
	if !reflect.DeepEqual(deps["d"], []string{"c", "b", "a"}) {
		t.Errorf("Expected 'd' to have 'c', 'b', 'a' deps, but found %v", deps["d"])
	}
	components = []*Component{
		&Component{Provides: []string{"a"}, Requires: []string{"b"}},
		&Component{Provides: []string{"b"}, Requires: []string{"a"}},
	}
	deps = BuildDeps(components)
	if !reflect.DeepEqual(deps["a"], []string{"b"}) || !reflect.DeepEqual(deps["b"], []string{"a"}) {
		t.Errorf("Expected to properly work with circular dependencies")
	}
	components = []*Component{
		&Component{Provides: []string{"a"}, Requires: []string{"b", "c", "d"}},
		&Component{Provides: []string{"e"}, Requires: []string{"a", "f"}},
		&Component{Provides: []string{"f"}, Requires: []string{"a", "b", "c"}},
	}
	deps = BuildDeps(components)
	if !reflect.DeepEqual(deps["e"], []string{"a", "b", "c", "d", "f"}) {
		t.Errorf("Expected to find dependencies for e, but got %v", deps)
	}
}
