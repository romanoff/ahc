package stylesheet

import (
	"bitbucket.org/romanoff/ahc/ti"
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

type Component struct {
	File            os.FileInfo
	Path            string
	Content         string
	CompiledContent string
	Provides        []string
	Requires        []string
	Preprocessor    string
	Templates       []string
	Font            string
}

func (self *Component) ModTime() (time time.Time, err error) {
	cssFile := self.File
	if cssFile == nil {
		err = errors.New("No file in component")
		return
	}
	return
}

func (self *Component) Compile(components []*Component, deps map[string][]string) error {
	if len(self.Provides) == 0 {
		return errors.New("Component doesn't provide anything")
	}
	componentRequirements := deps[self.Provides[0]]
	usedComponents := make([]*Component, 0)
	for _, requirement := range componentRequirements {
		for _, component := range components {
			usedComponents = addComponentIfMatches(requirement, component, usedComponents)
		}
	}
	content := ""
	for _, component := range usedComponents {
		content += "\n"
		content = component.Content + content
		if component.Preprocessor != self.Preprocessor {
			return errors.New(fmt.Sprintf("%v file and its dependency - %v have different preprocessors", self.Path, component.Path))
		}
	}
	content += self.Content
	self.CompiledContent = content
	if self.Preprocessor != "" {
		return self.PreprocessContent()
	}
	return nil
}

func (self *Component) PreprocessContent() error {
	tempfile, err := ioutil.TempFile("", "ahc")
	defer os.Remove(tempfile.Name())
	defer tempfile.Close()
	if err != nil {
		return err
	}
	_, err = tempfile.WriteString(self.CompiledContent)
	if err != nil {
		return err
	}
	name, arg, err := GetPreprocessorInfo(self.Preprocessor)
	if err != nil {
		return err
	}
	arg = append(arg, tempfile.Name())
	cmd := exec.Command(name, arg...)
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	self.CompiledContent = string(output)
	return nil
}

func (self *Component) Init() (err error) {
	bytesContent, err := ioutil.ReadFile(self.Path)
	if err != nil {
		return err
	}
	content := string(bytesContent)
	self.Content = content
	self.readAnnotations()
	err = self.replaceImagesWithDataUri()
	if err != nil {
		return err
	}
	return
}

func addComponentIfMatches(requirement string, component *Component, usedComponents []*Component) []*Component {
	for _, componentProvide := range component.Provides {
		if componentProvide == requirement {
			for _, usedComponent := range usedComponents {
				if usedComponent.Path == component.Path {
					return usedComponents
				}
			}
			usedComponents = append(usedComponents, component)
			return usedComponents
		}
	}
	return usedComponents
}

var provideRe *regexp.Regexp = regexp.MustCompile("@provide\\s+['\"](.+)['\"]")
var RequireRe *regexp.Regexp = regexp.MustCompile("@require\\s+['\"](.+)['\"]")
var FontRe *regexp.Regexp = regexp.MustCompile("@font\\s+['\"](.+)['\"]")
var preprocessorRe *regexp.Regexp = regexp.MustCompile("@preprocessor\\s+['\"](.+)['\"]")

func (self *Component) readAnnotations() {
	provides := make([]string, 0)
	requires := make([]string, 0)
	content := self.Content
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		matches := provideRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			provides = append(provides, matches[1])
		}
		matches = RequireRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			requires = append(requires, matches[1])
		}
		matches = preprocessorRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.Preprocessor = matches[1]
		}
		matches = FontRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.Font = matches[1]
		}

	}
	self.Provides = provides
	self.Requires = requires
	return
}

var backgroundImageRe *regexp.Regexp = regexp.MustCompile("background-image:\\s+url\\((.+)\\);")

func (self *Component) replaceImagesWithDataUri() error {
	lines := strings.Split(self.Content, "\n")

	for _, line := range lines {
		matches := backgroundImageRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			path := strings.Replace(self.Path, self.File.Name(), "", 1) + matches[1]
			contentBytes, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			dataUri := &bytes.Buffer{}
			encoder := base64.NewEncoder(base64.StdEncoding, dataUri)
			encoder.Write(contentBytes)
			encoder.Close()
			newLine := "background-image: url('data:image/png;base64," + dataUri.String() + "');"
			self.Content = strings.Replace(self.Content, line, newLine, 1)
		}
	}
	return nil
}

func (self *Component) GetHtml() string {
	htmlPath := strings.Replace(self.Path, ".css", ".html", 1)
	contentBytes, err := ioutil.ReadFile(htmlPath)
	if err != nil {
		return ""
	}
	return string(contentBytes)
}

func (self *Component) CheckTemplate(extension string) {
	templatePath := strings.Replace(self.Path, ".css", "."+extension, 1)
	if _, err := os.Stat(templatePath); err == nil {
		self.Templates = append(self.Templates, extension)
	}
}

func (self *Component) GetTemplateExec(templatesInfo []*ti.TemplateInfo) (string, string, error) {
	for _, templateInfo := range templatesInfo {
		for _, templateExtension := range self.Templates {
			if templateInfo.Extension == templateExtension {
				return templateInfo.Command, strings.Replace(self.Path, ".css", "."+templateExtension, 1), nil
			}
		}
	}
	return "", "", errors.New(fmt.Sprintf("No template found for %v", self.Provides[0]))
}

func FindComponent(components []*Component, templateName string) (*Component, error) {
	for _, component := range components {
		for _, provide := range component.Provides {
			if provide == templateName {
				return component, nil
			}
		}
	}
	return nil, errors.New(fmt.Sprintf("%v component not found", templateName))
}
