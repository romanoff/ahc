package stylesheet

type Deps map[string][]string

func BuildDeps(components []*Component) map[string][]string {
	deps := make(map[string][]string)
	depsReverse := make(map[string][]string)
	for _, component := range components {
		for _, provide := range component.Provides {
			for _, require := range component.Requires {
				deps, depsReverse = addDependency(deps, depsReverse, provide, require)
			}
		}
	}
	return deps
}

func addDependency(deps Deps, depsReverse Deps, provide string, require string) (Deps, Deps) {
	deps = addIfUniq(deps, provide, require)
	depsReverse = addIfUniq(depsReverse, require, provide)
	for _, usedIn := range depsReverse[provide] {
		deps = addIfUniq(deps, usedIn, require)
		depsReverse = addIfUniq(depsReverse, require, usedIn)
	}

	for _, required := range deps[require] {
		deps = addIfUniq(deps, provide, required)
		depsReverse = addIfUniq(depsReverse, required, provide)
	}
	return deps, depsReverse
}

func addIfUniq(deps Deps, key string, value string) Deps {
	if key == value {
		return deps
	}
	for _, depsValue := range deps[key] {
		if depsValue == value {
			return deps
		}
	}
	deps[key] = append(deps[key], value)
	return deps
}
