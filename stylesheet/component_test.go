package stylesheet

import (
	"reflect"
	"testing"
)

func TestAddComponentIfMatches(t *testing.T) {
	requirement := "goog.Button"
	component := &Component{Provides: []string{"goog.Some", "goog.Button"}, Path: "/some/path"}
	usedComponents := []*Component{}
	usedComponents = addComponentIfMatches(requirement, component, usedComponents)
	if !reflect.DeepEqual(usedComponents, []*Component{component}) {
		t.Errorf("Expected to add component to used components")
	}
	usedComponents = addComponentIfMatches(requirement, component, usedComponents)
	if !reflect.DeepEqual(usedComponents, []*Component{component}) {
		t.Errorf("Expected to get one component, but got %v", len(usedComponents))
	}
}
