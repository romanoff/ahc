package stylesheet

import (
	"errors"
	"io/ioutil"
	"os"
	"os/user"
	"regexp"
	"strings"
)

var preprocessorsInfo = make(map[string][]string)
var infoFetched = false

func GetPreprocessorInfo(preprocessor string) (name string, arg []string, err error) {
	if infoFetched == false {
		if err := fetchPreprocessorsInfo(); err != nil {
			return "", nil, err
		}
	}
	info := preprocessorsInfo[preprocessor]
	if len(info) > 0 {
		return info[0], info[1:], nil
	}
	return "", nil, errors.New("Preprocessor not found")
}

var re *regexp.Regexp = regexp.MustCompile("^\\s*(.+)\\s*:\\s*(.+)\\s*")

func fetchPreprocessorsInfo() error {
	infoFetched = true
	u, err := user.Current()
	if err != nil {
		return err
	}
	homeDir := u.HomeDir
	preprocessorsInfoPath := homeDir + "/.ahc/.preprocessors"
	if _, err = os.Stat(preprocessorsInfoPath); err != nil {
		return err
	}
	content, err := ioutil.ReadFile(preprocessorsInfoPath)
	lines := strings.Split(string(content), "\n")
	for _, line := range lines {
		matches := re.FindStringSubmatch(line)
		if len(matches) == 3 {
			preprocessorsInfo[matches[1]] = strings.Split(matches[2], " ")
		}
	}
	return nil
}
