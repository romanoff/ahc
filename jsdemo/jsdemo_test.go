package jsdemo

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"testing"
)

func TestParseJsDemo(t *testing.T) {
	content := `
/**
 * @title 'Product'
 * @css   'p.Product'
 * @deps  'p.ui.Product'
 * @deps  'goog.dom'
 * @initscript 'init();'
 * @body
 * <div id='some'></div>
 */
goog.require('p.ui.Product');
goog.require('goog.dom');

function init() {
    var body = goog.dom.getElementsByTagNameAndClass(goog.dom.TagName.BODY)[0];
    var product = new p.ui.Product({name: "Product name", url: "#", imageUrl: "http://placehold.it/50x50"}, {name: "Company", url: "#company"}, true);
    product.render(body);
};
`
	file := File{Content: content}
	file.ParseContent([]*stylesheet.Component{&stylesheet.Component{Provides: []string{"p.Product"}, CompiledContent: ".product{}"}})
	if file.Title != "Product" {
		t.Errorf("Expected to get 'Product' title, but got '%v'", file.Title)
	}
	if len(file.Stylesheets) != 1 || file.Stylesheets[0] != "p.Product" {
		t.Errorf("Expected to get 'p.Product' css, but got '%v'", file.Stylesheets)
	}
	if len(file.Deps) != 2 || file.Deps[0] != "p.ui.Product" || file.Deps[1] != "goog.dom" {
		t.Errorf("Expected to get 2 script deps, but got '%v'", file.Deps)
	}
	if file.InitScript != "init();" {
		t.Errorf("Expected to get 'init();' initscript, but got '%v'", file.InitScript)
	}
	if file.Css != ".product{}" {
		t.Errorf("Css is wrong")
	}
	if file.Body != "<div id='some'></div>" {
		t.Errorf("Body content is wrong")
	}
}
