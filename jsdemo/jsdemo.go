package jsdemo

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"io/ioutil"
	"regexp"
	"strings"
)

type File struct {
	Path        string
	Content     string
	Title       string
	Deps        []string
	EventTypes  []string
	Stylesheets []string
	Css         string
	InitScript  string
	Body        string
}

var titleRe *regexp.Regexp = regexp.MustCompile("@title\\s+['\"](.+)['\"]")
var cssRe *regexp.Regexp = regexp.MustCompile("@css\\s+['\"](.+)['\"]")
var depsRe *regexp.Regexp = regexp.MustCompile("@deps\\s+['\"](.+)['\"]")
var eventTypeRe *regexp.Regexp = regexp.MustCompile("@eventtype\\s+['\"](.+)['\"]")
var initScriptRe *regexp.Regexp = regexp.MustCompile("@initscript\\s+['\"](.+)['\"]")
var bodyStartRe *regexp.Regexp = regexp.MustCompile("@body")
var bodyConentRe *regexp.Regexp = regexp.MustCompile("\\*(.+)")
var bodyEndRe *regexp.Regexp = regexp.MustCompile("\\*/")

func (self *File) Parse(components []*stylesheet.Component) error {
	content, err := ioutil.ReadFile(self.Path)
	if err != nil {
		return err
	}
	self.Content = string(content)
	err = self.ParseContent(components)
	if err != nil {
		return err
	}
	return nil
}

func (self *File) ParseContent(components []*stylesheet.Component) error {
	lines := strings.Split(self.Content, "\n")
	bodyStarted := false
	for _, line := range lines {
		matches := titleRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.Title = matches[1]
		}
		matches = cssRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.Stylesheets = append(self.Stylesheets, matches[1])
		}
		matches = depsRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.Deps = append(self.Deps, matches[1])
		}
		matches = eventTypeRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.EventTypes = append(self.EventTypes, matches[1])
		}
		matches = initScriptRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			self.InitScript = matches[1]
		}
		matches = bodyEndRe.FindStringSubmatch(line)
		if len(matches) == 1 {
			bodyStarted = false
		}
		if bodyStarted {
			matches = bodyConentRe.FindStringSubmatch(line)
			if len(matches) == 2 {
				self.Body += matches[1]
			}
		}
		matches = bodyStartRe.FindStringSubmatch(line)
		if len(matches) == 1 {
			bodyStarted = true
		}
	}
	self.Body = strings.TrimSpace(self.Body)
	for _, component := range components {
		for _, provide := range component.Provides {
			for _, stylesheet := range self.Stylesheets {
				if provide == stylesheet {
					self.Css += component.CompiledContent
				}
			}
		}
	}
	return nil
}
