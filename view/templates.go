package view

import (
	"bitbucket.org/romanoff/ahc/ti"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"strings"
)

var templatesInfo = make([]*ti.TemplateInfo, 0, 0)

var infoFetched = false

func GetTemplatesInfo() ([]*ti.TemplateInfo, error) {
	if !infoFetched {
		if err := fetchTemplatesInfo(); err != nil {
			return nil, err
		}
	}
	return templatesInfo, nil
}

var re *regexp.Regexp = regexp.MustCompile("^\\s*([^:]+)\\s*:\\s*(.+)\\s*")

func fetchTemplatesInfo() error {
	u, err := user.Current()
	if err != nil {
		return err
	}
	homeDir := u.HomeDir
	templatesInfoPath := homeDir + "/.ahc/.templates"
	if _, err = os.Stat(templatesInfoPath); err != nil {
		return err
	}
	content, err := ioutil.ReadFile(templatesInfoPath)
	lines := strings.Split(string(content), "\n")
	for _, line := range lines {
		matches := re.FindStringSubmatch(line)
		if len(matches) == 3 {
			templatesInfo = append(templatesInfo, &ti.TemplateInfo{Extension: matches[1], Command: strings.TrimSpace(matches[2])})
		}
	}
	infoFetched = true
	return nil
}

func RenderTemplate(command, provide, templatePath, jsonString string) (string, error) {
	if strings.HasPrefix(command, ":") {
		requestUrl := "http://localhost" + command + "/?name=" + url.QueryEscape(provide)
		requestUrl += "&params=" + url.QueryEscape(jsonString)
		res, err := http.Get(requestUrl)
		if err != nil {
			return "", err
		}
		content, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			return "", err
		}
		return fmt.Sprintf("%s", content), nil
	}
	cmd := exec.Command(command, templatePath, jsonString)
	commandOutput, err := cmd.Output()
	return string(commandOutput), err
}
