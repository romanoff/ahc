package view

import (
	"errors"
	"fmt"
	"github.com/jteeuwen/go-pkg-xmlx"
	"io/ioutil"
	"os"
)

type Layout struct {
	Path         string
	File         os.FileInfo
	ContentBytes []byte
	XmlDocument  *xmlx.Document
	Name         string
}

func (self *Layout) LoadContent() error {
	contentBytes, err := ioutil.ReadFile(self.Path)
	if err != nil {
		return err
	}
	self.ContentBytes = contentBytes
	name := self.File.Name()
	self.Name = name[0 : len(name)-len(".layout")]
	return nil
}
func (self *Layout) GetXmlDocument(params map[string][]*xmlx.Node) (*xmlx.Document, error) {
	document := xmlx.New()
	document.LoadBytes(self.ContentBytes, nil)
	for _, node := range document.Root.Children {
		err := self.insertParams(node, params)
		if err != nil {
			return nil, err
		}
	}
	return document, nil
}

func (self *Layout) insertParams(node *xmlx.Node, params map[string][]*xmlx.Node) error {
	replaceIndexes := []int{}
	children := node.Children
	for i := len(children) - 1; i >= 0; i-- {
		if children[i].Name.Local == "yield" {
			replaceIndexes = append(replaceIndexes, i)
		}
		if len(children[i].Children) > 0 {
			self.insertParams(children[i], params)
		}
	}
	for _, i := range replaceIndexes {
		replacedNode := children[i]
		name := self.getNodeName(replacedNode)
		if name == "" {
			return errors.New(fmt.Sprintf("No name specified in yield for layout %v", self.Path))
		}
		newChildren := make([]*xmlx.Node, 0, 0)
		newChildren = append(newChildren, children[0:i]...)
		newChildren = append(newChildren, params[name]...)
		if i < len(children)-1 {
			newChildren = append(newChildren, children[i+1:len(children)-1]...)
		}
		node.Children = newChildren
	}
	return nil
}

func (self *Layout) getNodeName(node *xmlx.Node) string {
	for _, attribute := range node.Attributes {
		if attribute.Name.Local == "name" {
			return attribute.Value
		}
	}
	return ""
}
