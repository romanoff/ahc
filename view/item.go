package view

import (
	"bitbucket.org/romanoff/ahc/stylesheet"
	"bitbucket.org/romanoff/ahc/ti"
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jteeuwen/go-pkg-xmlx"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

type Item struct {
	Path           string
	File           os.FileInfo
	Html           string
	Content        string
	UsedComponents []string
	UsedPartials   []string
	Partial        bool
}

var globalCache map[string]string = make(map[string]string)

func (self *Item) GenerateHtml(layouts []*Layout, components []*stylesheet.Component, templatesInfo []*ti.TemplateInfo) error {
	return self.GenerateHtmlWithPartials(layouts, components, templatesInfo, map[string]string{})
}
func (self *Item) GenerateHtmlWithPartials(layouts []*Layout, components []*stylesheet.Component, templatesInfo []*ti.TemplateInfo, partials map[string]string) error {
	contentBytes, err := ioutil.ReadFile(self.Path)
	if err != nil {
		return err
	}
	document := xmlx.New()
	document.LoadBytes(contentBytes, nil)
	html, err := self.getComponentsHtml(layouts, document.Root.Children, components, templatesInfo, partials)
	if err != nil {
		return err
	}
	self.Html = html + self.GetScript()
	return nil
}

func (self *Item) GetScript() string {
	contentBytes, err := ioutil.ReadFile(self.Name() + ".script")
	if err != nil {
		return ""
	}
	return string(contentBytes)
}

func (self *Item) Name() string {
	return strings.Replace(self.File.Name(), ".view", "", 1)
}

func (self *Item) getComponentsHtml(layouts []*Layout, nodes []*xmlx.Node, components []*stylesheet.Component, templatesInfo []*ti.TemplateInfo, partials map[string]string) (string, error) {
	html := ""
	for _, node := range nodes {
		templateName := node.Name.Local

		if templateName == "layout" {
			layoutParams := make(map[string][]*xmlx.Node)
			for _, child := range node.Children {
				paramName := strings.TrimSpace(child.Name.Local)
				layoutParams[paramName] = child.Children
			}
			layoutName := ""
			for _, attribute := range node.Attributes {
				if attribute.Name.Local == "name" {
					layoutName = attribute.Value
				}
			}
			if layoutName == "" {
				return "", errors.New("No name attribute found for layout")
			}

			var selectedLayout *Layout = nil
			for _, layout := range layouts {
				if layout.Name == layoutName {
					selectedLayout = layout
				}
			}
			if selectedLayout == nil {
				return "", errors.New(fmt.Sprintf("No %v layout found", layoutName))
			}
			doc, err := selectedLayout.GetXmlDocument(layoutParams)
			if err != nil {
				return "", err
			}
			value, err := self.getComponentsHtml(layouts, doc.Root.Children, components, templatesInfo, partials)
			if err != nil {
				return "", err
			}
			html += value
			continue
		}

		params := make(map[string]string)

		for _, attribute := range node.Attributes {
			if attribute.Name.Local != "onclick" && attribute.Name.Local != "style" {
				params[attribute.Name.Local] = attribute.Value
			}
		}

		for _, param := range node.Children {
			if param.Name.Space != templateName {
				return "", errors.New(fmt.Sprintf("Expected %v:%v, but got %v:%v", templateName, param.Name.Local, param.Name.Space, param.Name.Local))
			}
			paramName := strings.TrimSpace(param.Name.Local)
			if paramName == "" {
				return "", errors.New(fmt.Sprintf("Expected to get parameter for %v, but got nothing", templateName))
			}
			if len(param.Children) > 0 {
				var value string
				if rawTrue(param) {
					for _, child := range param.Children {
						value += child.String()
					}
				} else {
					var err error
					value, err = self.getComponentsHtml(layouts, param.Children, components, templatesInfo, partials)
					if err != nil {
						return "", err
					}
				}
				params[paramName] = value
			} else {
				params[paramName] = param.Value
			}
		}

		if templateName == "plain" {
			html += node.Value
			continue
		}

		if templateName == "partial" {
			if partialHtml, ok := partials[params["name"]]; ok {
				html += partialHtml
				self.addToUsedPartials(params["name"])
				continue
			} else {
				return "", errors.New(fmt.Sprintf("%v partial not found. Used in %v ", params["name"], self.File.Name()))
			}
		}

		component, err := stylesheet.FindComponent(components, templateName)
		if err != nil {
			return "", err
		}
		self.addToUsed(component)
		command, templatePath, err := component.GetTemplateExec(templatesInfo)
		if err != nil {
			return "", err
		}
		jsonContent, err := json.Marshal(params)
		if err != nil {
			return "", err
		}
		// md5
		output := ""
		md5 := self.GetMd5(command + templatePath + string(jsonContent))
		if globalCache[md5] != "" {
			output = globalCache[md5]
		} else {
			commandOutput, err := RenderTemplate(command, component.Provides[0], templatePath, string(jsonContent))
			if err != nil {
				return "", err
			}
			output = string(commandOutput)
			globalCache[md5] = output
		}
		html += addAdditionalAttributes(node, strings.TrimSpace(output))
	}
	return html, nil
}

func (self *Item) addToUsed(component *stylesheet.Component) {
	provide := component.Provides[0]
	for _, used := range self.UsedComponents {
		if used == provide {
			return
		}
	}
	self.UsedComponents = append(self.UsedComponents, provide)
}

func (self *Item) addToUsedPartials(partialName string) {
	for _, partial := range self.UsedPartials {
		if partialName == partial {
			return
		}
	}
	self.UsedPartials = append(self.UsedPartials, partialName)
}

func (self *Item) GetMd5(content string) string {
	h := md5.New()
	io.WriteString(h, content)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func addAdditionalAttributes(node *xmlx.Node, output string) string {
	for _, attribute := range node.Attributes {
		if attribute.Name.Local == "style" {
			output = strings.Replace(output, ">", " style=\""+attribute.Value+"\" >", 1)
		}
	}
	for _, attribute := range node.Attributes {
		if attribute.Name.Local == "onclick" {
			output = strings.Replace(output, ">", " data-onclick=\""+attribute.Value+"\" >", 1)
		}
	}
	return output
}

func rawTrue(node *xmlx.Node) bool {
	for _, attribute := range node.Attributes {
		if attribute.Name.Local == "raw" && attribute.Value == "true" {
			return true
		}
	}
	return false
}
