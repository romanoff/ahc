package view

import (
	"github.com/jteeuwen/go-pkg-xmlx"
	"testing"
)

func TestParseContent(t *testing.T) {
	layoutContent := `
<div class="main">
<div class="content">
  <div class="bla"></div>
  <yield name="content" />
</div>
<yield name="c1" />
</div>
`

	content := `
<span>Content</span>
<span>Content1</span>
`

	c1 := `
<span>Content3</span>
<span>Content4</span>
`

	document := xmlx.New()
	document.LoadString(content, nil)

	document1 := xmlx.New()
	document1.LoadString(c1, nil)

	layout := &Layout{}
	layout.ContentBytes = []byte(layoutContent)

	m := make(map[string][]*xmlx.Node)
	m["content"] = document.Root.Children
	m["c1"] = document1.Root.Children

	doc, err := layout.GetXmlDocument(m)
	if err != nil {
		t.Errorf("Got error while rendering layout: %v", err)
	}
	expected := `<div class="main"><div class="content"><div class="bla" /><span>Content</span><span>Content1</span></div><span>Content3</span><span>Content4</span></div>`
	if value := doc.Root.Children[0].String(); value != expected {
		t.Errorf("\nExpected to get:\n%v\nafter rendeing layout, but got:\n%v", expected, value)
	}
}
