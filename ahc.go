package main

import "bitbucket.org/romanoff/ahc/action"

import (
	"fmt"
	"os"
)

func main() {
	err := action.PerformAction(os.Args[1:])
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
